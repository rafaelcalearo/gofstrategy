
package Service;

import java.util.Scanner;

public class Exemplo {

	public static void main(String[] args) {
		try (Scanner entrada = new Scanner(System.in)) {
			System.out.print("Informe o tamanho do tapete: ");
			int tamanho = entrada.nextInt();
			System.out.print("Qual o tipo de Lavagem (1) Lava,Seca e Perfume (2) Lava e Seca: ");
			int opcaoFrete = entrada.nextInt();
			TipoLavagem tipoLavagem = TipoLavagem.values()[opcaoFrete 	 - 1];
			
			Lavagem lavagem = new Lavagem(tipoLavagem);
			double preco = lavagem.calcularPreco(tamanho);
			System.out.printf("O valor total é de R$%.2f", preco);
		}
	}
	
}
