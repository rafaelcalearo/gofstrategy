/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.Funcionario;
import services.CalculaImposto;

/**
 *
 * @author Rafael
 */
public class CalculoContribuicaoOnzePorcento implements CalculaImposto {

    /**
     * QUEM GANHA DE R$ 2.822,91 ATE R$ 5.645,80/ACIMA DEVE TER DESCONTADO 11%
     * DO SEU SALARIO SEGUNDO A TABELA DE CONTRIBUICAO MENSAL A SER PAGO AO INSS
     * QUEM GANHA ACIMA DEVE POSSUIR UM "CARGO" DE GERENTE/SUPERVISOR
     */
    private final double aliquota = 11; //%
    private final double tetoMaximoSENIOR = 5645.80;

    @Override
    public double calcularContribuicaoAPagar(Funcionario funcionario) {
        if (funcionario.getSalarioBase() > tetoMaximoSENIOR) {
            throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
        }
        return Math.round(funcionario.getSalarioBase() * aliquota / 100);
    }
}
