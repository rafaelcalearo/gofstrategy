/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import model.Funcionario;

/**
 *
 * @author Rafael
 */
public interface CalculaImposto {
    double calculaSalarioComImposto(Funcionario umFuncionario);
}
