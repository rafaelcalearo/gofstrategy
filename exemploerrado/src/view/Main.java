package view;

import model.Funcionario;

/**
 *
 * @author Rafael
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Funcionario umFuncionario = new Funcionario(Funcionario.DESENVOLVEDOR,
                2100);
        System.out.println(umFuncionario.calcularSalarioComImposto());

        Funcionario outroFuncionario = new Funcionario(Funcionario.GERENTE,
                8700);
        System.out.println(outroFuncionario.calcularSalarioComImposto());
    }    
}
