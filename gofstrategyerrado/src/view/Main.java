package view;

import model.Funcionario;

/**
 *
 * @author Rafael
 */
public class Main {

    public static void main(String[] args) {
        Funcionario junior = new Funcionario(Funcionario.JUNIOR, 1500);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", junior.getSalarioBase(), junior.calcularContribuicaoAPagar()));
        
        Funcionario pleno = new Funcionario(Funcionario.PLENO, 2000.76);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", pleno.getSalarioBase(), pleno.calcularContribuicaoAPagar()));
        
        Funcionario senior = new Funcionario(Funcionario.SENIOR, 4590.50);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", senior.getSalarioBase(), senior.calcularContribuicaoAPagar()));
    }

}
