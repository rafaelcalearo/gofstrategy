# <img src="https://gitlab.com/rafaelcalearo/gofstrategy/raw/master/img/logo.png" width="30" /> GoF Strategy 

O projeto "GoF Strategy" busca mostrar o "[Strategy](https://en.wikipedia.org/wiki/Strategy_pattern)" um 
[Padrão de Projeto (Design Patterns)](https://pt.wikipedia.org/wiki/Padr%C3%A3o_de_projeto_de_software) 
que está exemplificado dentre os vários mostrados no livro chamado 
"**Design Patterns: Elements of Reusable Object-Oriented Software — Padrões de 
Projeto: Elementos de Software Orientado a Objetos Reutilizáveis**", que foi 
criado em 1995 pelos profissionais: [Erich Gamma](https://pt.wikipedia.org/wiki/Erich_Gamma), 
[Richard Helms](https://de.wikipedia.org/wiki/Richard_Helm), 
[Ralph Johnson](https://pt.wikipedia.org/wiki/Ralph_Johnson) e 
[John Vlissides](https://pt.wikipedia.org/wiki/John_Vlissides). 
Por esse feito, os profissionais foram batizados com o nome 
"Gangue dos Quatro" (Gang of Four ou GoF). 
Os **Design Patterns** foram divididos em 3 grupos:

- **Padrões de Criação:** relacionados à criação de objetos;
- **Padrões Estruturais:** tratam das associações entre classes e objetos;
- **Padrões Comportamentais:** tratam das interações e divisões de responsabilidades entre as classes.

O padrão [Strategy](https://en.wikipedia.org/wiki/Strategy_pattern) esta dentro 
dos padrões **comportamentais**, ele define uma família de algoritmos que 
possuem características em comum, [encapsula](https://pt.wikipedia.org/wiki/Encapsulamento_(inform%C3%A1tica)) 
cada um, e deixe-os intercambiáveis. O padrão [Strategy](https://en.wikipedia.org/wiki/Strategy_pattern) 
permite que o algorítimo varie independentemente dos clientes que o usam.

O GoF Strategy começou a ser desenvolvido na aula 14 (atividade única) 
e faz parte da avaliação da disciplina de **Engenharia de Software III** do 
4° semestre da [Faculdade de Tecnologia Senac Pelotas-RS](https://www.senacrs.com.br/unidades.asp?unidade=78) e
pertence ao grupo: **[Rafael Calearo](https://gitlab.com/rafaelcalearo)** e 
**[Matheus Borges](https://gitlab.com/mborges93)**.

🚧 **Situação atual do projeto:** em construção/desenvolvimento!

## Pré-requisitos

- Ter instalado a [Máquina virtual Java](https://pt.wikipedia.org/wiki/M%C3%A1quina_virtual_Java);
- Ter instalado o [JDK](https://pt.wikipedia.org/wiki/Java_Development_Kit); e
- Uma [IDE](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado) de sua preferência.

## Índice

- [Clonar](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md#Clonar)
- [How to (como usar)](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md) 
- [O grupo](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md)
- [Documentação](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md)
- [Licença](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md)


## Clonar

Para efetuar o clone do projeto usando os *links* disponíveis no repositório do 
[GitLab](https://docs.gitlab.com/) é preciso possuir o *software* **Git** 
instalado na sua máquina. Para saber como instalar e usar a ferramenta **Git** 
acesse o site do mesmo: <https://git-scm.com/doc>. Caso já saiba como fazer 
use os comandos abaixo:

### Com SSH

```bash
$ git clone git@gitlab.com:rafaelcalearo/gofstrategy.git
```

ou 

### Com HTTPS:

```bash
$ git clone https://gitlab.com/rafaelcalearo/gofstrategy.git
```

ou você pode baixar o projeto (*download*): [zip](https://gitlab.com/rafaelcalearo/gofstrategy/-/archive/master/gofstrategy-master.zip),
[tar.gz](https://gitlab.com/rafaelcalearo/gofstrategy/-/archive/master/gofstrategy-master.tar.gz),
[tar.bz2](https://gitlab.com/rafaelcalearo/gofstrategy/-/archive/master/gofstrategy-master.tar.bz2) e 
[tar](https://gitlab.com/rafaelcalearo/gofstrategy/-/archive/master/gofstrategy-master.tar).

### Uso

**ANTES DE USAR:** leia os [pré-requisitos](https://gitlab.com/rafaelcalearo/gofstrategy/blob/master/README.md#pré-requisitos).
Após ter clonado/baixado o projeto, use a opção de *import* na sua 
[IDE](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado) (abra-o) e 
execute o mesmo.

## Grupo

O nosso grupo conta com os seguintes membros (mantenedores):

**[Matheus Borges](https://gitlab.com/mborges93)**<br>
**[Rafael Calearo](https://gitlab.com/rafaelcalearo)** 

## Documentação

A documentação com maior clareza se encontra na 
**[wiki](https://gitlab.com/rafaelcalearo/gofstrategy/wikis/home)** 
desse repositório.

## Licença

A aplicação conta com a seguinte licença de uso: **[MIT](https://opensource.org/licenses/MIT)**.
