package services;

import models.Funcionario;

/**
 *
 * @author Rafael
 */
public interface CalculaImposto {
    double calcularContribuicaoAPagar(Funcionario funcionario);
}
