package model;

/**
 *
 * @author Rafael
 */
public class Funcionario {

    protected int cargo;
    public static final int JUNIOR = 1;
    public static final int PLENO = 2;
    public static final int SENIOR = 3;
    //protected int ESTAGIARIO = 4;    
    private final double salarioBase;
    protected double tetoMaximoJUNIOR = 1693.72;
    protected double tetoMaximoPLENO = 2822.90;
    protected double tetoMaximoSENIOR = 5645.80;
    protected double aliquota1 = 8;
    protected double aliquota2 = 9;
    protected double aliquota3 = 11;

    public Funcionario(int cargo, double salarioBase) {
        this.cargo = cargo;
        this.salarioBase = salarioBase;
    }

    public double calcularContribuicaoAPagar() {
        switch (cargo) {
            case JUNIOR:
                if (salarioBase > tetoMaximoJUNIOR) {
                    throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
                } else {
                    return salarioBase * aliquota1 / 100;
                }
            case PLENO:
                if (salarioBase > tetoMaximoPLENO) {
                    throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
                } else {
                    return salarioBase * aliquota2 / 100;
                }
            case SENIOR:
                if (salarioBase > tetoMaximoSENIOR) {
                    throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
                } else {
                    return salarioBase * aliquota3 / 100;
                }
            default:
                System.err.println("CARGO NÃO ENCONTRADO!");
        }
        return 0;
    }

    public double getSalarioBase() {
        return salarioBase;
    }
}
