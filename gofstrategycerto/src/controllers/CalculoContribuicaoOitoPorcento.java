package controllers;

import models.Funcionario;
import services.CalculaImposto;

/**
 *
 * @author Rafael
 */
public class CalculoContribuicaoOitoPorcento implements CalculaImposto {

    /**
     * QUEM GANHA ATE R$ 1.693,72 DEVE TER DESCONTADO 8% DO SEU SALARIO SEGUNDO
     * A TABELA DE CONTRIBUICAO MENSAL A SER PAGO AO INSS
     */
    private final double aliquota = 8; //%
    private final double tetoMaximoJUNIOR = 1693.72;

    @Override
    public double calcularContribuicaoAPagar(Funcionario funcionario) {
         if (funcionario.getSalarioBase() > tetoMaximoJUNIOR) {
            throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
        }
        return Math.round(funcionario.getSalarioBase() * aliquota / 100);
    }
}
