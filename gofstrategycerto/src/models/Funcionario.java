package models;

import controllers.CalculoContribuicaoNovePorcento;
import controllers.CalculoContribuicaoOitoPorcento;
import controllers.CalculoContribuicaoOnzePorcento;
import services.CalculaImposto;

/**
 *
 * @author Rafael
 */
public class Funcionario {

    public static final int JUNIOR = 1;
    public static final int PLENO = 2;
    public static final int SENIOR = 3;
    //public static final int ESTAGIARIO = 4;
    protected double salarioBase;
    protected int cargo;
    protected CalculaImposto calculo;

    public Funcionario(int cargo, double salarioBase) {
        this.salarioBase = salarioBase;
        switch (cargo) {
            case JUNIOR:
                calculo = new CalculoContribuicaoOitoPorcento();
                break;
            case PLENO:
                calculo = new CalculoContribuicaoNovePorcento();
                break;
            case SENIOR:
                calculo = new CalculoContribuicaoOnzePorcento();
                break;
            //case ESTAGIARIO:
            //  calculo = new CalculoContribuicaoDoisPorcento();               
            //  break;
            default:
                System.err.println("CARGO NÃO ENCONTRADO!");
        }
    }

    public double calcularContribuicaoAPagar() {
        return calculo.calcularContribuicaoAPagar(this);
    }

    public double getSalarioBase() {
        return salarioBase;
    }
}
