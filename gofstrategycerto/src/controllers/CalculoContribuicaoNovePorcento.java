package controllers;

import models.Funcionario;
import services.CalculaImposto;

/**
 *
 * @author Rafael
 */
public class CalculoContribuicaoNovePorcento implements CalculaImposto {

    /**
     * QUEM GANHA DE R$ 1.693,73 A R$ 2.822,90 DEVE TER DESCONTADO 9% DO SEU
     * SALARIO SEGUNDO A TABELA DE CONTRIBUICAO MENSAL A SER PAGO AO INSS
     */
    private final double aliquota = 9;
    private final double tetoMaximoPLENO = 2822.90;

    @Override
    public double calcularContribuicaoAPagar(Funcionario funcionario) {
        if (funcionario.getSalarioBase() > tetoMaximoPLENO) {
            throw new RuntimeException("SALARIO ACIMA DO MÁXIMO PERMITIDO POR ESSE CARGO!");
        }
        return Math.round(funcionario.getSalarioBase() * aliquota / 100);
    }
}
