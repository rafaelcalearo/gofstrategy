/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;
public class Lavagem {

	private TipoLavagem lav;
	
	public Lavagem(TipoLavagem lav) {
		this.lav = lav;
	}

	public double calcularPreco(int tam) {
		double preco = 0;
		if (TipoLavagem.PERF.equals(lav)) {
			preco = tam * 1.50 + 10;
		} else if (TipoLavagem.BASIC.equals(lav)) {
			preco = tam * 1.30;
		}
		
		return preco;
	}
	
}