package service;

import java.util.Scanner;

public class exemplo {

    public static void main(String[] args) {
        try (Scanner entrada = new Scanner(System.in)) {
            System.out.print("Informe o tamanho do tapete: ");
            int tamanho = entrada.nextInt();
            System.out.print("Qual o tipo de Lavagem (1)Lava,Seca e Perfume (2) Lava e Seca: ");
            int opcaoFrete = entrada.nextInt();
            TipoLavagem tipoLavagem = TipoLavagem.values()[opcaoFrete - 1];
            //devolve o tipo de lavagem
            Lavagem lavagem = tipoLavagem.obterLavagem();
            double preco = lavagem.calcularPreco(tamanho);
            System.out.printf("O valor total é de R$%.2f", preco);
        }
    }

}
