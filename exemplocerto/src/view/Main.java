package view;

import model.Funcionario;

/**
 *
 * @author Rafael
 */
public class Main {

    public static void main(String[] args) {
        Funcionario umFuncionario = new Funcionario(Funcionario.DESENVOLVEDOR,
                2100);
        System.out.println(umFuncionario.calcularSalarioComImposto());

        Funcionario outroFuncionario = new Funcionario(Funcionario.GERENTE,
                4700);
        System.out.println(outroFuncionario.calcularSalarioComImposto());
    }

}
