package controllers;

import models.Funcionario;
import services.CalculaImposto;

/**
 *
 * @author Rafael
 */
public class CalculoContribuicaoDoisPorcento implements CalculaImposto {

    /**
     * ESTAGIARIO QUE GANHA ATE R$ 954.00 DEVE TER DESCONTADO 2% DO SEU SALARIO
     * SEGUNDO A TABELA DE CONTRIBUICAO MENSAL A SER PAGO AO INSS, CASO O SEU
     * SALARIO SEJA MAIOR QUE O SALARIO MINIMO SENDO ESTAGIARIO O VALOR A SER
     * PAGO AO INSS SERA CONFORME A TABELA VIGENTE DESSE ANO: 2018
     */
    private final double aliquota1 = 2; //%
    private final double aliquota2 = 8; //%
    private final double aliquota3 = 9; //%
    private final double aliquota4 = 11; //%
    private final double tetoMaximoESTAGIARIO = 954.00;
    private final double tetoMaximoJUNIOR = 1693.72;
    private final double tetoMaximoPLENO = 2822.90;
    private final double tetoMaximoSENIOR = 5645.80;

    @Override
    public double calcularContribuicaoAPagar(Funcionario f) {
        if (f.getSalarioBase() > tetoMaximoESTAGIARIO
                && f.getSalarioBase() < tetoMaximoJUNIOR) {
            return Math.round(f.getSalarioBase() * aliquota2 / 100);
        } else if (f.getSalarioBase() > tetoMaximoJUNIOR
                && f.getSalarioBase() < tetoMaximoPLENO) {
            return Math.round(f.getSalarioBase() * aliquota3 / 100);
        } else if (f.getSalarioBase() > tetoMaximoPLENO
                && f.getSalarioBase() < tetoMaximoSENIOR) {
            return Math.round(f.getSalarioBase() * aliquota4 / 100);
        } else if (f.getSalarioBase() > tetoMaximoSENIOR) {
            return Math.round(tetoMaximoSENIOR * aliquota4 / 100);
        }
        return Math.round(f.getSalarioBase() * aliquota1 / 100);
    }
}
