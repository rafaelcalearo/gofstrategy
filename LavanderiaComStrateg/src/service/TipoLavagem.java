package service;

import service.lavagem.Basic;
import service.lavagem.Perf;

public enum TipoLavagem {
    
    //cria uma fabrica de objetos

    Perf {
        @Override
        public Lavagem obterLavagem() {
            return new Perf();
        }
    },
    Basic {
        @Override
        public Lavagem obterLavagem() {
            return new Basic();
        }
    };

    public abstract Lavagem obterLavagem();

}
