/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import models.Funcionario;

/**
 *
 * @author Rafael
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Funcionario junior = new Funcionario(Funcionario.JUNIOR, 1500);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", junior.getSalarioBase(), junior.calcularContribuicaoAPagar()));

        Funcionario pleno = new Funcionario(Funcionario.PLENO, 2023.45);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", pleno.getSalarioBase(), pleno.calcularContribuicaoAPagar()));

        Funcionario senior = new Funcionario(Funcionario.SENIOR, 3549.56);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", senior.getSalarioBase(), senior.calcularContribuicaoAPagar()));
        
        /**Funcionario estagiario = new Funcionario(Funcionario.ESTAGIARIO, 2000.99);

        System.out.println(String.format("No decorrido mês foi deduzido do seu "
                + "salário de R$ %.2f a quantidade de R$ %.2f para o recolhimento do "
                + "INSS!", estagiario.getSalarioBase(), estagiario.calcularContribuicaoAPagar()));*/
    }
}
